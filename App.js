import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  AlertIOS
} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Item, Input, Text, Title, Content, List, ListItem, Thumbnail } from 'native-base';
import axios from 'axios';
import YouTube from 'react-native-youtube';

const api_key = "AIzaSyACuor0-KL-H4nak1HEGzPokP7vz1e6KjA";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchString: "",
      videos: [],
      currentVideo: ""
    };

    this.videoSearch = this.videoSearch.bind(this);
    this.renderVideo = this.renderVideo.bind(this);
    this.renderSearchList = this.renderSearchList.bind(this);
    this.onItemPress = this.onItemPress.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  videoSearch() {
    axios.get(`https://www.googleapis.com/youtube/v3/search?part=snippet&type=youtube%23video&key=${api_key}&q=${this.state.searchString}&maxResults=20`)
      .then((resp) => {
        console.log(resp)
        this.setState({ videos: resp.data.items });
      })
      .catch((error) => {
      });
  }

  onSearchChange(value) {
    const v = value.toLowerCase()
    if (v.includes("buba") || v.includes("jala")) {
      this.setState({searchString: "Imaj ukusa"})
    } else {
      this.setState({searchString: value});
    }
  }

  onPressLearnMore() {
    AlertIOS.alert(
      'Sync Complete',
      'All your data are belong to us.'
    );
  }

  onItemPress(videoId) {
    this.setState({
      currentVideo: videoId
    });
  }

  renderVideo() {
    if (this.state.currentVideo) {
      return (
        <YouTube
          videoId={this.state.currentVideo}
          // play={true}
          // fullscreen={true}
          loop={true}
          onReady={e => this.setState({ isReady: true })}
          onChangeState={e => this.setState({ status: e.state })}
          onChangeQuality={e => this.setState({ quality: e.quality })}
          onError={e => this.setState({ error: e.error })}
          style={{ alignSelf: "stretch", height: 300, shadowRadius: 10, shadowColor: "#ccc", shadowOpacity: 1 }}
        />
      );
    }
    return null;
  }

  renderSearchList() {
    if (this.state.videos.length) {
      return (
        this.state.videos.map((video, index) => (
          <ListItem onPress={() => { this.onItemPress(video.id.videoId) }} button key={video.id.videoId}>
            <Thumbnail square size={80} source={{ uri: video.snippet.thumbnails.default.url }} />
            <Body>
              <Text>{video.snippet.title}</Text>
              <Text note>{video.snippet.description}</Text>
            </Body>
          </ListItem>
        ))
      );
    }
    return (
      <Text>Search results will appear here.</Text>
    )
  }

  render() {
    return (
      <Container>
        <Header searchBar rounded style={{ backgroundColor: "pink" }}>
          <Item >
            <Icon name="ios-search" />
            <Input placeholder="Search" value={this.state.searchString} onChangeText={this.onSearchChange} />
          </Item>
          <Button transparent onPress={this.videoSearch}>
            <Text>Search</Text>
          </Button>
        </Header>

        <Container>
          {this.renderVideo()}
          <Content style={{ backgroundColor: "pink" }}>
            <List>
              {this.renderSearchList()}
            </List>
          </Content>
        </Container>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  
});
